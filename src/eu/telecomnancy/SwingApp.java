package eu.telecomnancy;

import eu.telecomnancy.sensor.AlternativeTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorDecoratorAverage;
import eu.telecomnancy.sensor.SensorFactoryAlternativeTemperatureSencor;
import eu.telecomnancy.sensor.SensorProxy;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        ISensor proxytest = new SensorProxy(sensor);
        ISensor deco = new SensorDecoratorAverage(proxytest);
        
        new MainWindow(deco);
    }

}
