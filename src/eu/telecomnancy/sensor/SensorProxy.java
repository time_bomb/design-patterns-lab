package eu.telecomnancy.sensor;

import java.util.Date;
import java.util.Observable;

public class SensorProxy extends Observable implements ISensor{

	/**
	 * @uml.property  name="capteur"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	ISensor capteur;
	/**
	 * @uml.property  name="date"
	 */
	Date date;
	
	public SensorProxy(ISensor capteur){
		this.capteur = capteur;
	}
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		date = new Date();
		System.out.println("[Log]"+ date.getDate()+"/"+ date.getMonth()+"/"+ date.getDay()+ " "+ date.getHours()+ ":"+ date.getMinutes()+" On ");
		capteur.on();
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		date = new Date();
		System.out.println("[Log]"+ date.getDate()+"/"+ date.getMonth()+"/"+ date.getDay()+ " "+ date.getHours()+ ":"+ date.getMinutes()+" Off ");
		capteur.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		boolean result = capteur.getStatus();
		
		date = new Date();
		System.out.println("[Log]"+ date.getDate()+"/"+ date.getMonth()+"/"+ date.getDay()+ " "+ date.getHours()+ ":"+ date.getMinutes()+" getStatus " + result);
		return result;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		date = new Date();
		System.out.println("[Log]"+ date.getDate()+"/"+ date.getMonth()+"/"+ date.getDay()+ " "+ date.getHours()+ ":"+ date.getMinutes()+" update ");
		capteur.update();
		this.setChanged();
    	this.notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		double result = capteur.getValue();
		date = new Date();
		System.out.println("[Log]"+ date.getDate()+"/"+ date.getMonth()+"/"+ date.getDay()+ " "+ date.getHours()+ ":"+ date.getMinutes()+" getValue " + result);
		return result;
	}

}
