package eu.telecomnancy.sensor;

public class SensorDecoratorFahrenheit extends SensorDecorator{

	public SensorDecoratorFahrenheit(ISensor capteur){
		super();
		this.capteur = capteur;
	}
	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		double valuecelsius = capteur.getValue();
		
		return (valuecelsius*1.8) + 32;
	}

}
