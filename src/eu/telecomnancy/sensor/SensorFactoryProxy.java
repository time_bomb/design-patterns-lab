package eu.telecomnancy.sensor;

public class SensorFactoryProxy extends SensorFactory{
	
	ISensor create(){
		return new SensorProxy(new TemperatureSensor());
	}
}
