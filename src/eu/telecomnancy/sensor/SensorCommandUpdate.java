package eu.telecomnancy.sensor;

public class SensorCommandUpdate extends SensorCommand{
	
	public SensorCommandUpdate(ISensor capteur){
		super();
		this.capteur = capteur;
	}
	
	public void execute(){
		try {
			capteur.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
