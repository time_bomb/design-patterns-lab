package eu.telecomnancy.sensor;

public class AdapteurLegacyTemperaturSensor implements ISensor{

	/**
	 * @uml.property  name="adaptee"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private LegacyTemperatureSensor adaptee;
	/**
	 * @uml.property  name="value"
	 */
	private double value;
	
	public AdapteurLegacyTemperaturSensor(LegacyTemperatureSensor adaptee){
		this.adaptee = adaptee;
	}
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		if (adaptee.getStatus() == false){
			adaptee.onOff();
		}
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		if (adaptee.getStatus() == true){
			adaptee.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return adaptee.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		adaptee.onOff();
		if (!adaptee.getStatus()){
			throw new SensorNotActivatedException("message");
		}else{
			adaptee.onOff();
			adaptee.onOff();
			value = adaptee.getTemperature();
		}
		
	}

	/**
	 * @return
	 * @throws SensorNotActivatedException
	 * @uml.property  name="value"
	 */
	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return value;
	}

}
