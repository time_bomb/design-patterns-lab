package eu.telecomnancy.sensor;

public class SensorFactoryTemperatureSensor extends SensorFactory{
	ISensor create(){
		return new TemperatureSensor();
	}
}
