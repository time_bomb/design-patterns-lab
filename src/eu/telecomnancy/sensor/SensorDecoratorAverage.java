package eu.telecomnancy.sensor;

public class SensorDecoratorAverage extends SensorDecorator{
	
	public SensorDecoratorAverage (ISensor capteur){
		super();
		this.capteur = capteur;
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		double valuecelsius = capteur.getValue();
		
		return Math.round(valuecelsius);
	}
}
