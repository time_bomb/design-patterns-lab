package eu.telecomnancy.sensor;

public class SensorCommandOff extends SensorCommand{
	
	public SensorCommandOff(ISensor capteur){
		super();
		this.capteur = capteur;
	}
	
	public void execute(){
		capteur.off();
	}
}