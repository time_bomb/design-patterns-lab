package eu.telecomnancy.sensor;

public class SensorCommandOn extends SensorCommand{
	
	public SensorCommandOn(ISensor capteur){
		super();
		this.capteur = capteur;
	}
	
	public void execute(){
		capteur.on();
	}
}
