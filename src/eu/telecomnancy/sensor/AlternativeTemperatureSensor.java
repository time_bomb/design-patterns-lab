package eu.telecomnancy.sensor;

import java.util.Observable;

public class AlternativeTemperatureSensor extends Observable implements ISensor {

	/**
	 * @uml.property  name="etat"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	SensorStat etat;

	public AlternativeTemperatureSensor(){
		this.etat =new SensorStatOff();
	}
	
    @Override
    public void on() {
        etat = new SensorStatOn();
    }

    @Override
    public void off() {
    	etat = new SensorStatOff();
    }

    @Override
    public boolean getStatus() {
        return etat.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	this.setChanged();
    	this.notifyObservers();
    	etat.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	return etat.getValue();
    }

}
