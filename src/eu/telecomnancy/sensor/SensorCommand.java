package eu.telecomnancy.sensor;

public abstract class SensorCommand {
	/**
	 * @uml.property  name="capteur"
	 * @uml.associationEnd  
	 */
	ISensor capteur;
	
	public abstract void execute();
}
