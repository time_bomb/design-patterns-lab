package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorStatOn extends SensorStat{
	
	/**
	 * @uml.property  name="value"
	 */
	double value = 0;
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
        value = (new Random()).nextDouble() * 100;
		
	}

	/**
	 * @return
	 * @throws SensorNotActivatedException
	 * @uml.property  name="value"
	 */
	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return value;
	}

}
