package eu.telecomnancy.sensor;

import java.util.Observable;

public abstract class SensorDecorator extends Observable implements ISensor {
	/**
	 * @uml.property  name="capteur"
	 * @uml.associationEnd  
	 */
	ISensor capteur;
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		capteur.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		capteur.off();
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return capteur.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		capteur.update();
		this.setChanged();
    	this.notifyObservers();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
