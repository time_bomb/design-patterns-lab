package eu.telecomnancy;
import eu.telecomnancy.sensor.AdapteurLegacyTemperaturSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;



public class Appnew {

    public static void main(String[] args) {
    	LegacyTemperatureSensor sensor = new LegacyTemperatureSensor();
    	ISensor adapteur = new AdapteurLegacyTemperaturSensor(sensor);
        new ConsoleUI(adapteur);
    }
}
